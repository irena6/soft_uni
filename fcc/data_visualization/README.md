## Data Visualization

- [**Visualize Data with a Bar Chart**](https://bomholtm.github.io/fcc/data_visualization/bar_chart)

  [![](../_assets/README/bar_chart.png)](https://bomholtm.github.io/fcc/data_visualization/bar_chart)

  <sup>_#html, #css, #javascript, #babel, #d3js, #reactjs_</sup>

- [**Visualize Data with a Scatterplot Graph**](https://bomholtm.github.io/fcc/data_visualization/scatter_plot)

  [![](../_assets/README/scatter_plot.png)](https://bomholtm.github.io/fcc/data_visualization/scatter_plot)

  <sup>_#html, #css, #javascript, #babel, #d3js, #reactjs_</sup>

- [**Visualize Data with a Heat Map**](https://bomholtm.github.io/fcc/data_visualization/heat_map)

  [![](../_assets/README/heat_map.png)](https://bomholtm.github.io/fcc/data_visualization/heat_map)

  <sup>_#html, #css, #javascript, #babel, #d3js, #reactjs_</sup>

- Visualize Data with a Choropleth Map

- Visualize Data with a Treemap Diagram
