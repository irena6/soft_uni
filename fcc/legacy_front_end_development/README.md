## Legacy Front End Development

- [**Build a Tribute Page**](https://bomholtm.github.io/fcc/legacy_front_end_development/tribute_page)

  [![](../_assets/README/tribute_page.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/tribute_page)

  <sup>_#html, #css, #sass, #bootstrap_</sup>

- [**Build a Personal Portfolio Webpage**](https://bomholtm.github.io/fcc/legacy_front_end_development/personal_portfolio)

  [![](../_assets/README/personal_portfolio.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/personal_portfolio)

  <sup>_#html, #css, #sass, #javascript, #jquery, #bootstrap_</sup>

- [**Build a Random Quote Machine**](https://bomholtm.github.io/fcc/legacy_front_end_development/random_quote_machine)

  [![](../_assets/README/random_quote_machine.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/random_quote_machine)

  <sup>_#html, #css, #sass, #javascript, #jquery, #zurb-foundation_</sup>

- [**Show the Local Weather**](https://bomholtm.github.io/fcc/legacy_front_end_development/local_weather)

  [![](../_assets/README/local_weather.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/local_weather)

  <sup>_#html, #css, #sass, #javascript, #jquery, #bootstrap_</sup>

- [**Build a Wikipedia Viewer**](https://bomholtm.github.io/fcc/legacy_front_end_development/wikipedia_viewer)

  [![](../_assets/README/wikipedia_viewer.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/wikipedia_viewer)

  <sup>_#html, #css, #sass, #javascript, #jquery, #zurb-foundation_</sup>

- [**Use the Twitch.tv JSON API**](https://bomholtm.github.io/fcc/legacy_front_end_development/twitch_status)

  [![](../_assets/README/twitch_status.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/twitch_status)

  <sup>_#html, #css, #sass, #javascript, #jquery, #bootstrap_</sup>

- [**Build a JavaScript Calculator**](https://bomholtm.github.io/fcc/legacy_front_end_development/js_calculator)

  [![](../_assets/README/js_calculator.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/js_calculator)

  <sup>_#html, #css, #sass, #javascript, #bootstrap_</sup>

- [**Build a Pomodoro Clock**](https://bomholtm.github.io/fcc/legacy_front_end_development/pomodoro_clock)

  [![](../_assets/README/pomodoro_clock.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/pomodoro_clock)

  <sup>_#html, #css, #sass, #javascript, #jquery, #zurb-foundation_</sup>

- [**Build a Tic Tac Toe Game**](https://bomholtm.github.io/fcc/legacy_front_end_development/tic_tac_toe)

  [![](../_assets/README/tic_tac_toe.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/tic_tac_toe)

  <sup>_#html, #css, #sass, #javascript, #jquery, #bootstrap_</sup>

- [**Build a Simon Game**](https://bomholtm.github.io/fcc/legacy_front_end_development/simon_game)

  [![](../_assets/README/simon_game.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/simon_game)

  <sup>_#html, #css, #sass, #javascript, #jquery_</sup>
