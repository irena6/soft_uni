## Responsive Web Design

:warning: **Please note:**

<sup>I originally solved the _"Build a Tribute Page"_ & _"Build a Personal Portfolio Webpage"_ projects as part of the Legacy Front End Development Certification. User stories may have changed, `code` can be found [here](https://github.com/bomholtm/fcc/tree/master/legacy_front_end_development).</sup>

- [**Build a Tribute Page**](https://bomholtm.github.io/fcc/legacy_front_end_development/tribute_page)

  [![](../_assets/README/tribute_page.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/tribute_page)

  <sup>_#html, #css, #sass, #bootstrap_</sup>

- [**Build a Survey Form**](https://bomholtm.github.io/fcc/responsive_web_design/survey_form)

  [![](../_assets/README/survey_form.png)](https://bomholtm.github.io/fcc/responsive_web_design/survey_form)

  <sup>_#html, #css, #sass, #javascript, #jquery, #materialize_</sup>

- [**Build A Product Landing Page**](https://bomholtm.github.io/fcc/responsive_web_design/landing_page)

  [![](../_assets/README/landing_page.png)](https://bomholtm.github.io/fcc/responsive_web_design/landing_page)

  <sup>_#html, #css, #sass, #javascript, #jquery, #bootstrap_</sup>

- [**Build a Technical Documentation Page**](https://bomholtm.github.io/fcc/responsive_web_design/documentation_page)

  [![](../_assets/README/documentation_page.png)](https://bomholtm.github.io/fcc/responsive_web_design/documentation_page)

  <sup>_#html, #css, #sass, #javascript, #jquery, #bootstrap_</sup>

- [**Build a Personal Portfolio Webpage**](https://bomholtm.github.io/fcc/legacy_front_end_development/personal_portfolio)

  [![](../_assets/README/personal_portfolio.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/personal_portfolio)

  <sup>_#html, #css, #sass, #javascript, #jquery, #bootstrap_</sup>
