/*
Each new term in the Fibonacci sequence is generated by adding the previous two terms.
By starting with 1 and 2, the first 10 terms will be: 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
By considering the terms in the Fibonacci sequence whose values do not exceed nth term, find the sum of the even-valued terms.
*/

function fiboEvenSum(n) {
  const fibo = [1, 2];
  let evenSum = fibo[1];
  function fibonacci(n) {
    if (n <= 1) {
      return fibo[n];
    } else if (fibo[n]) {
      return fibo[n];
    } else {
      fibo[n] = fibonacci(n - 1) + fibonacci(n - 2);
      if (fibo[n] % 2 == 0) {
        evenSum += fibo[n];
      }
      return fibo[n];
    }
  }
  fibonacci(n);
  return evenSum;
}

fiboEvenSum(10);
fiboEvenSum(23);
fiboEvenSum(43);
