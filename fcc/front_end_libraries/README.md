## Front End Libraries

###### :warning: Please note:

<sup>_"Build a Random Quote Machine"_, _"Build a JavaScript Calculator"_ & _"Build a Pomodoro Clock"_ were originally solved as part of the Legacy Front End Development Certification. The user stories may have changed, `code` can be found [here](https://github.com/bomholtm/fcc/tree/master/legacy_front_end_development).</sup>

- [**Build a Random Quote Machine**](https://bomholtm.github.io/fcc/legacy_front_end_development/random_quote_machine)

  [![](../_assets/README/random_quote_machine.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/random_quote_machine)

  <sup>_#html, #css, #sass, #javascript, #jquery, #zurb-foundation_</sup>

- [**Build A Markdown Previewer**](https://bomholtm.github.io/fcc/front_end_libraries/markdown_previewer)

  [![](../_assets/README/markdown_previewer.png)](https://bomholtm.github.io/fcc/front_end_libraries/markdown_previewer)

  <sup>_#html, #css, #javascript, #babel, #reactjs_</sup>

- Build A Drum Machine

- [**Build a JavaScript Calculator**](https://bomholtm.github.io/fcc/legacy_front_end_development/js_calculator)

  [![](../_assets/README/js_calculator.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/js_calculator)

  <sup>_#html, #css, #sass, #javascript, #bootstrap_</sup>

- [**Build a Pomodoro Clock**](https://bomholtm.github.io/fcc/legacy_front_end_development/pomodoro_clock)

  [![](../_assets/README/pomodoro_clock.png)](https://bomholtm.github.io/fcc/legacy_front_end_development/pomodoro_clock)

  <sup>_#html, #css, #sass, #javascript, #jquery, #zurb-foundation_</sup>
