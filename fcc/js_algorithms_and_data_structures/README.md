## JavaScript Algorithms and Data Structures

### Basic Algorithm Scripting

- [**Convert Celsius to Fahrenheit**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/convert_celsius_to_fahrenheit.js)
- [**Reverse a String**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/reverse_a_string.js)
- [**Factorialize a Number**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/factorialize_a_number.js)
- [**Find the Longest Word in a String**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/find_the_longest_word_in_a_string.js)
- [**Return Largest Numbers in Arrays**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/return_largest_numbers_in_arrays.js)
- [**Confirm the Ending**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/confirm_the_ending.js)
- [**Repeat a String Repeat a String**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/repeat_a_string_repeat_a_string.js)
- [**Truncate a String**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/truncate_a_string.js)
- [**Finders Keepers**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/finders_keepers.js)
- [**Boo who**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/boo_who.js)
- [**Title Case a Sentence**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/title_case_a_sentence.js)
- [**Slice and Splice**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/slice_and_splice.js)
- [**Falsy Bouncer**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/falsy_bouncer.js)
- [**Where do I Belong**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/where_do_i_belong.js)
- [**Mutations**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/mutations.js)
- [**Chunky Monkey**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/basic_algorithm_scripting/chunky_monkey.js)

### Intermediate Algorithm Scripting

- [**Sum All Numbers in a Range**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/sum_all_numbers_in_a_range.js)
- [**Diff Two Arrays**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/diff_two_arrays.js)
- [**Seek and Destroy**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/seek_and_destroy.js)
- [**Wherefore art thou**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/wherefore_art_thou.js)
- [**Spinal Tap Case**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/spinal_tap_case.js)
- [**Pig Latin**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/pig_latin.js)
- [**Search and Replace**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/search_and_replace.js)
- [**DNA Pairing**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/dna_pairing.js)
- [**Missing letters**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/missing_letters.js)
- [**Sorted Union**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/sorted_union.js)
- [**Convert HTML Entities**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/convert_html_entities.js)
- [**Sum All Odd Fibonacci Numbers**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/sum_all_odd_fibonacci_numbers.js)
- [**Sum All Primes**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/sum_all_primes.js)
- [**Smallest Common Multiple**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/smallest_common_multiple.js)
- [**Drop it**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/drop_it.js)
- [**Steamroller**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/steamroller.js)
- [**Binary Agents**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/binary_agents.js)
- [**Everything Be True**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/everything_be_true.js)
- [**Arguments Optional**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/arguments_optional.js)
- [**Make a Person**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/make_a_person.js)
- [**Map the Debris**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/intermediate_algorithm_scripting/map_the_debris.js)

### Advanced Algorithm Scripting

- [**Palindrome Checker**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/advanced_algorithm_scripting/palindrome_checker.js)
- [**Roman Numeral Converter**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/advanced_algorithm_scripting/roman_numeral_converter.js)
- [**Caesars Cipher**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/advanced_algorithm_scripting/caesars_cipher.js)
- [**Telephone Number Validator**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/advanced_algorithm_scripting/telephone_number_validator.js)
- [**Cash Register**](https://github.com/bomholtm/fcc/tree/master/js_algorithms_and_data_structures/advanced_algorithm_scripting/cash_register.js)
