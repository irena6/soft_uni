

## Course: [JavaScript Fundamentals](https://softuni.bg/trainings/js-fundamentals-january-2018)
SoftUni course JavaScript Fundamentals - January 2018: All tasks with their solutions.

## Course content:
- Syntax and Operators
- Document Object Model
- Functions and Logic Flow
- Arrays and Matrices
- Strings and RegExp
- Objects and JSON. DOM Events

## JS Programming Basics Course:
JavaScripts Programming Basics Course
